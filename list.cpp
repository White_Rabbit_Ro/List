#include "list.hpp"

List::~List()
{ //деструктор
    while (head != nullptr) {
        listbox* temp = head->next; //создаем узел, равный второму от главного
        delete head; //удаляем корень(голову) списка
        head = temp; //меняем адрес на следующий
    }
}

void Add(listbox*& curr, int val)
{ //функция с рекурсией для правильного заполнения списка
    if (curr == nullptr) {
        curr = new listbox; //новый узел
        curr->x = val;
        curr->next = nullptr;
    }
    else {
        Add(curr->next, val); //шагаем по списку
    }
}

void List::insert_items(int val)
{ //функция добавления элемента в список
    Add(head, val);
}

void List::Print() const
{ //функция вывода списка
    listbox* temp = head; //указатель на начало списка
    while (temp != nullptr) {
        cout << temp->x;
        if (temp->next != nullptr)
            cout << " -> ";
        temp = temp->next;
    }
    cout << endl;
}

bool List::Remove(int x)
{ //функция удаления элемента
    if(head->x == x) {
        listbox* ex = head->next;
        delete head;
        head = ex;
        return true;
    }
    listbox* ex = head;
    while(ex->next != nullptr && ex->next->x != x)
        ex = ex->next;
    if(ex->next==nullptr) {
        return false;
    }
    listbox* ptr = ex->next;
    ex->next = ex->next->next;
    delete ptr;
    return true;
}

void List::Search(int val) const
{ //поиск позиции элемента
    cout << "\"" << val << "\": ";
    listbox* ex = head; //для хождения по списку
    bool b = true; //проверка наличия элемента
    int pos; //счетчик
    
    while (ex != nullptr) {
        if (ex->x == val) {
            cout << pos << " ";
            b = false;
        }
        ex = ex->next;
        pos++;
    }
    
    if (b)
        cout << "Элемент не найден" << endl;
    else
        cout << endl;
}

bool List::Change(int pos, int val)
{ //изменить элемент по позиции
    listbox* ex = head;
    for (unsigned int i = 0; i < pos; i++) {
        if (ex->next == nullptr) {
            return false;
        }
        ex = ex->next;
    }
    ex->x = val;
    return true;
}

void List::Sort() const
{ //сортировка по возрастанию
    for (listbox* one = head; one; one = one->next) {
        for (listbox* two = head; two; two = two->next) {
            if (two->x > one->x)
                swap(two->x, one->x);
        }
    }
}

void Operation(List box, bool b)
{
    cout << "1. Распечатать список\n2. Добавить элементы в список\n3. Удалить элемент\n4. Найти позиции элементов\n5. Заменить элемент на другой\n6. Отсортировать элементы списка\n7. Завершить работу программы\n";
    
    int ch;
    cin >> ch;
    switch (ch) {
        case 1: { //распечатать список
            if (b == true)
                cout << "Список пуст\n";
            else
                box.Print();
        } break;
        case 2: { //добавить элементы
            cout << "Введите элементы: ";
            int val = 0;
            cin >> val;
            
            if (cin) { //ввод нескольких элементов через буфер
                box.insert_items(val);
                while (cin.get() != '\n') {
                    if (!(cin >> val)) {
                        cin.clear();
                        while (cin.get() != '\n')
                            cout << "НЕКОРЕКТНОЕ ЗНАЧЕНИЕ" << endl;
                        break;
                    }
                    box.insert_items(val);
                }
            }
            else {
                cin.clear();
                while (cin.get() != '\n')
                    ;
                cout << "НЕКОРЕКТНОЕ ЗНАЧЕНИЕ" << endl;
            }
            
        } break;
        case 3: {
            cout << "Введите значение элемента: ";
            int val;
            cin >> val;
            bool bRem = box.Remove(val);
            
            if (bRem == true)
                cout << "SUCCESS\n";
            else
                cout << "NO ELEMENT\n";
        } break;
        case 4: {
            cout << "Введите элементы: ";
            int val = 0;
            cin >> val;
            
            if (cin) { //ввод нескольких элементов через буфер
                box.Search(val);
                while (cin.get() != '\n') {
                    if (!(cin >> val)) {
                        cin.clear();
                        while (cin.get() != '\n')
                            cout << "НЕКОРЕКТНОЕ ЗНАЧЕНИЕ" << endl;
                        break;
                    }
                    box.Search(val);
                }
            }
            else {
                cin.clear();
                while (cin.get() != '\n')
                    ;
                cout << "НЕКОРЕКТНОЕ ЗНАЧЕНИЕ" << endl;
            }
        } break;
        case 5: {
            cout << "Введите позицию и значение элемента: ";
            int pos;
            cin >> pos;
            int val;
            cin >> val;
            if (!cin) {
                cin.clear();
                while (cin.get() != '\n')
                    ;
                cout << "Введено некорректное значение!" << endl;
                break;
            }
            bool f = box.Change(pos, val);
            if (!f)
                cout << "Элемент с позицией " << pos << " не существует!" << endl;
        } break;
        case 6:
            box.Sort();
            break;
        case 7: { //выходим или не выходим по 'y'/'n', иначе 'ОШИБКА'
            string cExit;
            cout << "Вы хотите выйти? (Y/N): ";
            cin >> cExit;
            
            if (cExit == "y" || cExit == "Y" || cExit == "yes" || cExit == "Yes" || cExit == "YES") {
                cout << "До свидания!\n";
                exit(0);
            }
            else if (cExit == "n" || cExit == "N" || cExit == "no" || cExit == "No" || cExit == "NO")
                break;
            else
                cout << "Введено не y/n";
        } break;
    }
    Operation(box, b); //рекурсия
}
