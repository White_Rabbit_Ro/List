using namespace std;

struct listbox {
    int x;
    listbox* next;
};

class List { //список элементов целочисленного типа
    listbox* head;
    
public:
    List() { head = nullptr; }; //инициализация указателя пустым значением(конструктор)
    void Print() const; //функция вывода списка на экран
    void insert_items(int); //функция добавления элемента
    bool Remove(int); //функция удаления элемента (полное совпадение)
    void Search(int) const; //поиск позиции элемента
    bool Change(int, int); //изменить элемент по его позиции
    void Sort() const; //сортировка по возрастанию
    ~List(); //деструктор
};