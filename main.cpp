#include <iostream>
#include <string>
#include "list.cpp"

void Operation(List, bool); //выбор операции

int main(int argc, char* argv[])
{
    setlocale(LC_ALL, "Rus");
    
    bool empty; //для пустого списка
    List page; //наш рабочий список
    
    if (argc > 2) { //если пробел
        for (unsigned int i = 1; i < argc; i++)
            page.insert_items(atoi(argv[i]));
        empty = false;
    }
    else if (argc == 2) { //если запятые
        char* nBegin = argv[1];
        for (;;) {
            page.insert_items(atoi(nBegin));
            if (strchr(nBegin, ',') == 0)
                break; // если строка дошла до конца - выйти
            nBegin = strchr(nBegin, ',') + 1; // двигаемся по последовательности, пропуская ','
        }
        empty = false;
    }
    else
        empty = true;
    
    Operation(page, empty);
}
